package ru.bobep.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.jtattoo.plaf.graphite.GraphiteLookAndFeel;

import ru.bobep.listener.ButnListener;

public class Frame extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5505601279740431857L;

	private final JPanel contentPane = new JPanel();

	private final JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
	private final PanelImport panelImport = new PanelImport();
	private final PanelExport panelExport = new PanelExport();

	/**
	 * @wbp.nonvisual location=14,249
	 */
	private final JFileChooser fileChooser = new JFileChooser();
	private ButnListener butnListener;

	public Frame() {

		this.setContentPane(contentPane);

		initialize();

		this.setTitle("VPR");
		this.setResizable(false);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setSize(new Dimension(830, 190));

		try {
			this.setIconImage(
					Toolkit.getDefaultToolkit().getImage(getClass().getResource("/resources/icon/compareIcon.png")));
		} catch (NullPointerException e) {
			this.setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/icon/compareIcon.png")));
		}
		this.setVisible(true);
	}

	private void initialize() {

		contentPane.setLayout(new BorderLayout(0, 0));
		contentPane.add(tabbedPane, BorderLayout.CENTER);
		tabbedPane.addTab("Импортировать", null, panelImport, null);
		tabbedPane.addTab("Сравнить и Экспортировать", null, panelExport, null);

		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

		butnListener = new ButnListener(fileChooser, this, panelImport, panelExport);
		panelImport.getBtnChoseFile().addActionListener(butnListener);
		panelImport.getBtnImport().addActionListener(butnListener);
		panelExport.getBtnCompareAndExport().addActionListener(butnListener);
	}

	// private void setOtherProperties() {
	//
	// fileChooser.removeChoosableFileFilter(fileChooser.getFileFilter());
	// fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
	// fileChooser.addChoosableFileFilter(excelFilterxls);
	// fileChooser.addChoosableFileFilter(excelFilterxlsx);
	//
	// }

	public static void main(String[] args) {
		long startTime = System.nanoTime();

		UIManager.put("OptionPane.messageFont", new Font("Times New Roman", Font.PLAIN, 18));
		UIManager.put("OptionPane.buttonFont", new Font("Times New Roman", Font.PLAIN, 18));

		try {
			UIManager.setLookAndFeel(new GraphiteLookAndFeel());
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}

		JFrame.setDefaultLookAndFeelDecorated(true);

		@SuppressWarnings("unused")
		Frame frame = new Frame();
		System.out.println((System.nanoTime() - startTime) / 1000000000.0);
	}
}
