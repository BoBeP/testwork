/**
 * 
 */
package ru.bobep.gui;

import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import com.toedter.calendar.JDateChooser;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class PanelImport extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1031507715939983427L;

	private final JButton btnChoseFile = new JButton("Выберите файл");
	private final JLabel lblPatchToFile = new JLabel("");
	private final JDateChooser dtchrImport = new JDateChooser();
	private final JButton btnImport = new JButton("Импортировать");

	public PanelImport() {
		initialize();
	}

	private void initialize() {

		dtchrImport.getCalendarButton().setText("Выберите дату для импорта");
		lblPatchToFile.setBorder(new TitledBorder(new LineBorder(new Color(144, 144, 144)), "Путь до файла",
				TitledBorder.LEFT, TitledBorder.TOP, null, new Color(0, 0, 0)));

		dtchrImport.getCalendarButton().setFont(new Font("Times New Roman", Font.PLAIN, 14));
		dtchrImport.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		lblPatchToFile.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		btnChoseFile.setFont(new Font("Times New Roman", Font.PLAIN, 14));

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout
				.setHorizontalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup().addGap(10)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addGroup(groupLayout.createSequentialGroup()
												.addComponent(dtchrImport, GroupLayout.DEFAULT_SIZE, 329,
														Short.MAX_VALUE)
												.addGap(15).addComponent(btnImport, GroupLayout.DEFAULT_SIZE, 288,
														Short.MAX_VALUE))
						.addGroup(groupLayout.createSequentialGroup()
								.addComponent(btnChoseFile, GroupLayout.DEFAULT_SIZE, 122, Short.MAX_VALUE).addGap(15)
								.addComponent(lblPatchToFile, GroupLayout.DEFAULT_SIZE, 473, Short.MAX_VALUE)))
				.addGap(10)));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup().addGap(10)
						.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addComponent(lblPatchToFile, GroupLayout.PREFERRED_SIZE, 45,
										GroupLayout.PREFERRED_SIZE)
						.addComponent(btnChoseFile, GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE))
				.addGap(10)
				.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
						.addComponent(btnImport, GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE)
						.addComponent(dtchrImport, GroupLayout.DEFAULT_SIZE, 45, Short.MAX_VALUE)).addGap(10)));
		setLayout(groupLayout);
	}

	/**
	 * 
	 * @return Возвращает кнопку выбора файла
	 */
	public JButton getBtnChoseFile() {
		return btnChoseFile;
	}

	/**
	 * 
	 * @return Возвращает кнопку импорта
	 */
	public JButton getBtnImport() {
		return btnImport;
	}

	/**
	 * 
	 * @return Возвращает компонент для выбора даты
	 */
	public JDateChooser getDtchrImport() {
		return dtchrImport;
	}

	/**
	 * 
	 * @return возращает поля для вывода файла
	 */
	public JLabel getLblPatchToFile() {
		return lblPatchToFile;
	}

}
