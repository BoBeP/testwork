package ru.bobep.gui;

import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JPanel;

import com.toedter.calendar.JDateChooser;

public class PanelExport extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6592688415519840062L;

	private final JDateChooser dtchrExportA = new JDateChooser();
	private final JDateChooser dtchrExportB = new JDateChooser();
	private final JButton btnCompareAndExport = new JButton("Сравнить и экспортировать");

	public PanelExport() {

		initialize();
	}

	private void initialize() {

		btnCompareAndExport.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		dtchrExportB.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		dtchrExportA.setFont(new Font("Times New Roman", Font.PLAIN, 14));
		dtchrExportB.getCalendarButton().setFont(new Font("Times New Roman", Font.PLAIN, 14));
		dtchrExportA.getCalendarButton().setFont(new Font("Times New Roman", Font.PLAIN, 14));
		dtchrExportB.getCalendarButton().setText("Дата 2");
		dtchrExportA.getCalendarButton().setText("Дата 1");

		GroupLayout groupLayout = new GroupLayout(this);
		groupLayout
				.setHorizontalGroup(
						groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createSequentialGroup().addContainerGap()
										.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
												.addComponent(dtchrExportA, GroupLayout.DEFAULT_SIZE, 318,
														Short.MAX_VALUE)
										.addComponent(dtchrExportB, GroupLayout.DEFAULT_SIZE, 318, Short.MAX_VALUE))
						.addGap(15).addComponent(btnCompareAndExport, GroupLayout.DEFAULT_SIZE, 318, Short.MAX_VALUE)
						.addContainerGap()));
		groupLayout
				.setVerticalGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup().addGap(10)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
										.addComponent(btnCompareAndExport, GroupLayout.PREFERRED_SIZE, 100,
												GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createSequentialGroup()
										.addComponent(dtchrExportA, GroupLayout.PREFERRED_SIZE, 45,
												GroupLayout.PREFERRED_SIZE)
										.addGap(10).addComponent(dtchrExportB, GroupLayout.PREFERRED_SIZE, 45,
												GroupLayout.PREFERRED_SIZE)))
								.addGap(10)));
		setLayout(groupLayout);
	}

	/**
	 * 
	 * @return Возвращает кнопку сравнения данных
	 */
	public JButton getBtnCompareAndExport() {
		return btnCompareAndExport;
	}

	/**
	 * 
	 * @return Возвращает компонент для выбора даты
	 */
	public JDateChooser getDtchrExportA() {
		return dtchrExportA;
	}

	/**
	 * 
	 * @return Возвращает компонент для выбора даты
	 */
	public JDateChooser getDtchrExportB() {
		return dtchrExportB;
	}
}
