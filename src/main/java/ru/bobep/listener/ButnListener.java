package ru.bobep.listener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.LinkedList;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;

import org.apache.commons.io.FilenameUtils;

import ru.bobep.gui.PanelExport;
import ru.bobep.gui.PanelImport;
import ru.bobep.system.FileFilterM;

public class ButnListener implements ActionListener {

	private JFileChooser fileChooser;
	private JFrame frame;
	private PanelImport panelImport;
	private PanelExport panelExport;

	private FileFilterM xlsxFileFilter = new FileFilterM("xlsx", "Книга Excel");
	private FileFilterM xlsFileFilter = new FileFilterM("xls", "Книга Excel 97-2003");

	private XLSXWorker xlsxWorker = new XLSXWorker();
	private XLSWorker xlsWorker = new XLSWorker();

	private DBWorker dbWorker = new DBWorker();

	String patchToFile = null;

	public ButnListener(JFileChooser fileChooser, JFrame frame, PanelImport panelImport, PanelExport panelExport) {
		this.fileChooser = fileChooser;
		this.frame = frame;
		this.panelImport = panelImport;
		this.panelExport = panelExport;

	}

	private void removeFileFilter() {
		for (FileFilter delF : fileChooser.getChoosableFileFilters())
			fileChooser.removeChoosableFileFilter(delF);

		fileChooser.removeChoosableFileFilter(fileChooser.getFileFilter());
	}

	private String addFile() {
		removeFileFilter();
		fileChooser.addChoosableFileFilter(xlsxFileFilter);
		fileChooser.addChoosableFileFilter(xlsFileFilter);

		if (fileChooser.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION) {

			File file = fileChooser.getSelectedFile();

			panelImport.getLblPatchToFile().setText(file.getAbsolutePath());
			panelImport.getLblPatchToFile().setToolTipText(file.getAbsolutePath());

			return file.getAbsolutePath();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private String importInDB(String patchToFile) {

		if (panelImport.getDtchrImport().getDate() != null) {

			switch (FilenameUtils.getExtension(patchToFile)) {
			case "xlsx":

				String res_1 = xlsxWorker.openFile(patchToFile);
				if (res_1.equals("OK")) {

					Object[] res_2 = xlsxWorker.getDataFromExcel();

					if (res_2[0].equals("OK")) {

						if (dbWorker.openConnection().equals("OK")) {

							if (dbWorker.importInDB(((LinkedList<LinkedList<Object>>) res_2[1]),
									panelImport.getDtchrImport().getDate()).equals("OK")) {

								return "OK";
							} else {
								return "Возникла ошибка при импортирование. Попробуйте снова, если ошибка повторилась сновоа обратитесь к разработчику.";
							}
						} else {
							return "Возникла ошибка при импортирование. Попробуйте снова, если ошибка повторилась сновоа обратитесь к разработчику.";
						}
					} else {
						return "Возникла ошибка при импортирование. Попробуйте снова, если ошибка повторилась сновоа обратитесь к разработчику.";
					}
				} else {
					return "Возникла ошибка при импортирование. Попробуйте снова, если ошибка повторилась сновоа обратитесь к разработчику.";
				}

			case "xls":

				String res_11 = xlsWorker.openFile(patchToFile);
				if (res_11.equals("OK")) {

					Object[] res_2 = xlsWorker.getDataFromExcel();

					if (res_2[0].equals("OK")) {

						if (dbWorker.openConnection().equals("OK")) {

							if (dbWorker.importInDB((LinkedList<LinkedList<Object>>) res_2[1],
									panelImport.getDtchrImport().getDate()).equals("OK")) {

								return "OK";
							} else {
								return "Возникла ошибка при импортирование. Попробуйте снова, если ошибка повторилась сновоа обратитесь к разработчику.";
							}
						} else {
							return "Возникла ошибка при импортирование. Попробуйте снова, если ошибка повторилась сновоа обратитесь к разработчику.";
						}
					} else {
						return "Возникла ошибка при импортирование. Попробуйте снова, если ошибка повторилась сновоа обратитесь к разработчику.";
					}
				} else {
					return "Возникла ошибка при импортирование. Попробуйте снова, если ошибка повторилась сновоа обратитесь к разработчику.";
				}

			default:
				break;
			}

		} else {
			return "Дата не выбрана";
		}
		return "Данные не импортировались";
	}

	private String saveFile() {

		removeFileFilter();
		fileChooser.addChoosableFileFilter(xlsxFileFilter);
		fileChooser.addChoosableFileFilter(xlsFileFilter);
		fileChooser.setSelectedFile(null);
		if (panelExport.getDtchrExportA().getDate() != null && panelExport.getDtchrExportB().getDate() != null) {

			if (fileChooser.showSaveDialog(frame) == JFileChooser.APPROVE_OPTION) {

				String pathcToFile = (FilenameUtils.getExtension(fileChooser.getSelectedFile().getPath()) != null
						&& FilenameUtils.getExtension(fileChooser.getSelectedFile().getPath())
								.equals(((FileFilterM) fileChooser.getFileFilter()).getExtension()))
										? fileChooser.getSelectedFile().getPath()
										: fileChooser.getSelectedFile().getPath() + "."
												+ ((FileFilterM) fileChooser.getFileFilter()).getExtension();

				if (new File(pathcToFile).exists()) {

					switch (JOptionPane.showConfirmDialog(frame, "Файл существует, перезаписать?", "Перезаписать?",
							JOptionPane.YES_NO_CANCEL_OPTION)) {

					case JOptionPane.YES_OPTION:
						fileChooser.approveSelection();
						break;

					case JOptionPane.NO_OPTION:
						saveFile();
						break;

					case JOptionPane.CANCEL_OPTION:

						fileChooser.cancelSelection();
						return "CANCEL_OPTION";
					}

				}

				// String patchToFile =
				// (FilenameUtils.getExtension(selectedFile.getPath()) != null
				// && FilenameUtils.getExtension(selectedFile.getPath())
				// .equals(((FileFilterM)
				// fileChooser.getFileFilter()).getExtension()))
				// ? selectedFile.getPath()
				// : selectedFile.getPath() + "."
				// + ((FileFilterM) fileChooser.getFileFilter()).getExtension();

				return pathcToFile;

			} else {
				return "Cancle";
			}

		} else {
			return "Не выбрана дата";
		}
	}

	@SuppressWarnings("unchecked")
	private String exportFromDB(String patchToFile) {

		if (FilenameUtils.wildcardMatchOnSystem(patchToFile, "*." + xlsFileFilter.getExtension())
				|| FilenameUtils.wildcardMatchOnSystem(patchToFile, "*." + xlsxFileFilter.getExtension())) {
			switch (FilenameUtils.getExtension(patchToFile)) {

			case "xlsx":

				String exportQuery = "SELECT tw_report.id, tw_type_work.name_work, tw_material.name_material, tw_unit.name_unit, tw_report.amount, tw_material.price_per_unit, tw_report.cost, tw_report.cost_with_VAT,tw_report.type_home, tw_report.date FROM tw_report INNER JOIN tw_type_work ON tw_report.type_work = tw_type_work.id INNER JOIN tw_material ON tw_report.material = tw_material.id AND tw_report.price_per_unit = tw_material.id INNER JOIN tw_unit ON tw_report.unit = tw_unit.id ";

				Object[] resA = dbWorker.exportFromDB(exportQuery + "WHERE tw_report.date = '"
						+ new java.sql.Date(panelExport.getDtchrExportA().getDate().getTime()) + "'");
				Object[] resB = dbWorker.exportFromDB(exportQuery + "WHERE tw_report.date = '"
						+ new java.sql.Date(panelExport.getDtchrExportB().getDate().getTime()) + "'");
				if (((LinkedList<LinkedList<Object>>) resA[1]).size() > 0
						&& ((LinkedList<LinkedList<Object>>) resB[1]).size() > 0) {
					if (resA[0].equals("OK") && resB[0].equals("OK")) {
						Object[] resC = compareData((LinkedList<LinkedList<Object>>) resA[1],
								(LinkedList<LinkedList<Object>>) resB[1]);
						if (resC[0].equals("OK")) {
							if (xlsxWorker.setDataToExcel((LinkedList<LinkedList<Object>>) resC[1], patchToFile)
									.equals("OK"))
								return "Данные экспортировались";
						} else {
							return "Возникла ошибка при экспортирование. Попробуйте снова, если ошибка повторилась сновоа обратитесь к разработчику.";
						}
					} else {
						return "Возникла ошибка при экспортирование. Попробуйте снова, если ошибка повторилась сновоа обратитесь к разработчику.";
					}
				} else {
					if (((LinkedList<LinkedList<Object>>) resA[1]).size() == 0) {

						return "По дате" + new SimpleDateFormat("dd.MM.yyyy")
								.format(panelExport.getDtchrExportA().getDate().getTime()) + " данных не найденно";
					}
					if (((LinkedList<LinkedList<Object>>) resB[1]).size() == 0) {
						return "По дате" + new SimpleDateFormat("dd.MM.yyyy")
								.format(panelExport.getDtchrExportB().getDate().getTime()) + " данных не найденно";
					}
				}
			case "xls":

				String exportQuery1 = "SELECT tw_report.id, tw_type_work.name_work, tw_material.name_material, tw_unit.name_unit, tw_report.amount, tw_material.price_per_unit, tw_report.cost, tw_report.cost_with_VAT,tw_report.type_home, tw_report.date FROM tw_report INNER JOIN tw_type_work ON tw_report.type_work = tw_type_work.id INNER JOIN tw_material ON tw_report.material = tw_material.id AND tw_report.price_per_unit = tw_material.id INNER JOIN tw_unit ON tw_report.unit = tw_unit.id ";

				Object[] resA1 = dbWorker.exportFromDB(exportQuery1 + "WHERE tw_report.date = '"
						+ new java.sql.Date(panelExport.getDtchrExportA().getDate().getTime()) + "'");
				Object[] resB1 = dbWorker.exportFromDB(exportQuery1 + "WHERE tw_report.date = '"
						+ new java.sql.Date(panelExport.getDtchrExportB().getDate().getTime()) + "'");

				if (resA1[0].equals("OK") && resB1[0].equals("OK")) {
					Object[] resC = compareData((LinkedList<LinkedList<Object>>) resA1[1],
							(LinkedList<LinkedList<Object>>) resB1[1]);
					if (resC[0].equals("OK")) {
						if (xlsWorker.setDataToExcel((LinkedList<LinkedList<Object>>) resC[1], patchToFile)
								.equals("OK"))
							return "Данные экспортировались";
					} else {
						return "Возникла ошибка при экспортирование. Попробуйте снова, если ошибка повторилась сновоа обратитесь к разработчику.";
					}
				} else {
					return "Возникла ошибка при экспортирование. Попробуйте снова, если ошибка повторилась сновоа обратитесь к разработчику.";
				}
			default:
				break;
			}

			return patchToFile;

		} else {
			if (patchToFile.equals("CANCEL_OPTION"))
				return patchToFile;

			if (patchToFile.equals("ERROR"))
				return "Возникла ошибка при экспортирование. Попробуйте снова, если ошибка повторилась сновоа обратитесь к разработчику.";

			return patchToFile;
		}
	}

	private Object[] compareData(LinkedList<LinkedList<Object>> matrix_A, LinkedList<LinkedList<Object>> matrix_B) {

		LinkedList<LinkedList<Object>> compare = new LinkedList<>();

		for (LinkedList<Object> arrayList_one : matrix_A) {

			for (LinkedList<Object> arrayList_two : matrix_B) {

				if (arrayList_two.contains(arrayList_one.get(1)) && arrayList_two.contains(arrayList_one.get(2))) {

					int a = Integer.compare((int) arrayList_one.get(4), (int) arrayList_two.get(4));
					if (a > 0 || a < 0) {
						if (compare.contains(arrayList_one)) {
						} else
							compare.add(arrayList_one);
						if (compare.contains(arrayList_two)) {
						} else
							compare.add(arrayList_two);
						continue;
					}

					int b = Double.compare((double) arrayList_one.get(5), (double) arrayList_two.get(5));
					if (b > 0 || b < 0) {
						if (compare.contains(arrayList_one)) {
						} else
							compare.add(arrayList_one);
						if (compare.contains(arrayList_two)) {
						} else
							compare.add(arrayList_two);
						continue;
					}

					int c = Double.compare((double) arrayList_one.get(6), (double) arrayList_two.get(6));
					if (c > 0 || c < 0) {
						if (compare.contains(arrayList_one)) {
						} else
							compare.add(arrayList_one);
						if (compare.contains(arrayList_two)) {
						} else
							compare.add(arrayList_two);
						continue;
					}
					int d = Double.compare((double) arrayList_one.get(7), (double) arrayList_two.get(7));
					if (d > 0 || d < 0) {
						if (compare.contains(arrayList_one)) {
						} else
							compare.add(arrayList_one);
						if (compare.contains(arrayList_two)) {
						} else
							compare.add(arrayList_two);
						continue;
					}

				}
			}
		}

		return new Object[] { "OK", compare };

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		long startTime4 = System.nanoTime();

		switch (e.getActionCommand()) {
		case "Выберите файл":
			long startTime = System.nanoTime();
			patchToFile = addFile();
			System.out.println("From Выберите файл= " + (System.nanoTime() - startTime) / 1000000000.0);
			break;

		case "Импортировать":
			long startTime2 = System.nanoTime();

			if (patchToFile != null) {
				String res = importInDB(patchToFile);
				if (res.length() > 0 && res.equals("OK")) {
					JOptionPane.showMessageDialog(frame, "Данные импортировались", "Information",
							JOptionPane.INFORMATION_MESSAGE);
					patchToFile = null;
					panelImport.getLblPatchToFile().setText("");
					panelImport.getDtchrImport().setDate(null);

				} else {
					JOptionPane.showMessageDialog(frame, res, "Error", JOptionPane.ERROR_MESSAGE);
					break;
				}

			} else {
				JOptionPane.showMessageDialog(frame, "Не выбран файл", "Error", JOptionPane.ERROR_MESSAGE);
				break;
			}
			System.out.println("From Импортировать в бд= " + (System.nanoTime() - startTime2) / 1000000000.0);
			break;

		case "Сравнить и экспортировать":
			long startTime3 = System.nanoTime();
			String res = exportFromDB(saveFile());
			if (res.equals("Данные экспортировались")) {
				panelExport.getDtchrExportA().setDate(null);
				panelExport.getDtchrExportB().setDate(null);
				JOptionPane.showMessageDialog(frame, res, "Error", JOptionPane.INFORMATION_MESSAGE);
			} else {

				if (res.equals("CANCEL_OPTION")) {
					break;
				}
				JOptionPane.showMessageDialog(frame, res, "Error", JOptionPane.ERROR_MESSAGE);
			}
			System.out.println("From Сравнить и экспортировать= " + (System.nanoTime() - startTime3) / 1000000000.0);
			break;

		default:
			break;
		}
		System.out.println("From full= " + (System.nanoTime() - startTime4) / 1000000000.0);

	}

}
