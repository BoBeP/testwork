package ru.bobep.listener;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

public class DBWorker {
	private Connection connection;
	private static final String url_database = "jdbc:mysql://127.0.0.1:3306/testWork?autoReconnect=true";
	private static final String user_database = "BoBeP";
	private static final String password_database = "efbDQtXyC6wLuhER";

	private LinkedList<Object> arrayQueryS;
	private LinkedList<LinkedList<Object>> arrayQueryF = new LinkedList<>();

	private PreparedStatement prpdStmt_type_work;
	private PreparedStatement prpdStmt_tw_unit;
	private PreparedStatement prpdStmt_tw_material;
	private PreparedStatement prpdStmt_tw_material_2;
	private PreparedStatement prpdStmt_tw_report;

	private Statement st_type_work;
	private Statement st_unit;
	private Statement statement_select_report;

	private ResultSet resultSet_select_report;
	private ResultSet rs_type_work;
	private ResultSet rs_unit;
	private ResultSet rs_tw_material;

	private int id_work;

	private static final String INSERT_TW_TYPE_WORK = "insert into tw_type_work (name_work) values (?)";
	private static final String INSERT_TW_UNIT = "insert into tw_unit (name_unit) values (?)";
	private static final String INSERT_TW_MATERIAL = "insert into tw_material (name_material, price_per_unit, unit_id) "
			+ "values (?,?,?)";
	private static final String INSERT_TW_REPORT = "insert into tw_report (type_work, material, unit,amount, price_per_unit, cost,cost_with_VAT, type_home,date) "
			+ "values (?,?,?,?,?,?,?,?,?)";

	private static String SELECT_ID_WORK = "select * from tw_type_work where name_work ='";
	private static String SELECT_ID_UNIT = "select * from tw_unit where name_unit ='";
	private static final String SELECT_DUPLICATE = "select * from tw_material where name_material = ? AND price_per_unit = ?";

	public String openConnection() {
		try {
			if (connection == null || connection.isClosed())
				connection = (Connection) DriverManager.getConnection(url_database, user_database, password_database);
			return "OK";
		} catch (SQLException e) {

			e.printStackTrace();
			return "ERROR";
		}

	}

	public String importInDB(LinkedList<LinkedList<Object>> sheetData, Date date) {
		String typeHome = new String();
		try {

			try {
				if (connection == null || connection.isClosed())
					connection = (Connection) DriverManager.getConnection(url_database, user_database,
							password_database);
			} catch (SQLException e) {

				e.printStackTrace();
				return "ERROR";
			}

			if (connection.getAutoCommit())
				connection.setAutoCommit(false);
			prpdStmt_type_work = (PreparedStatement) connection.prepareStatement(INSERT_TW_TYPE_WORK);
			prpdStmt_tw_unit = (PreparedStatement) connection.prepareStatement(INSERT_TW_UNIT);
			prpdStmt_tw_material = (PreparedStatement) connection.prepareStatement(INSERT_TW_MATERIAL);
			prpdStmt_tw_report = (PreparedStatement) connection.prepareStatement(INSERT_TW_REPORT);

			st_type_work = (Statement) connection.createStatement();
			st_unit = (Statement) connection.createStatement();

			for (int i = 0; i < sheetData.size(); i++) {
				LinkedList<Object> data = sheetData.get(i);
				arrayQueryS = new LinkedList<>();
				SELECT_ID_WORK = "select * from tw_type_work where name_work ='";
				SELECT_ID_UNIT = "select * from tw_unit where name_unit ='";

				if (i == 0 || i == 1 || i == 2) {
					if (i == 1) {
						typeHome = (String) data.get(0);
						continue;
					}
					continue;
				}

				try {
					Double.parseDouble(data.get(0).toString());
				} catch (NumberFormatException e) {
					if (data.size() == 1) {
						try {
							prpdStmt_type_work.setString(1, (String) data.get(0));
							prpdStmt_type_work.execute();
							connection.commit();

							SELECT_ID_WORK += (String) data.get(0) + "'";

							rs_type_work = st_type_work.executeQuery(SELECT_ID_WORK);
							rs_type_work.next();
							id_work = rs_type_work.getInt("id");

						} catch (SQLException e1) {
							if (e1.getErrorCode() == 1062) {

								try {
									SELECT_ID_WORK += (String) data.get(0) + "'";

									rs_type_work = st_type_work.executeQuery(SELECT_ID_WORK);
									rs_type_work.next();
									id_work = rs_type_work.getInt("id");
								} catch (SQLException e2) {
									e2.printStackTrace();
								}
							} else {
								e1.printStackTrace();
							}
						}
					}
				}

				if (data.size() >= 3) {
					if (data.size() > 3) {

						try {
							prpdStmt_tw_unit.setString(1, (String) data.get(2));
							prpdStmt_tw_unit.execute();
							connection.commit();
						} catch (SQLException e1) {
							if (e1.getErrorCode() != 1062)
								e1.printStackTrace();
						}

						try {

							SELECT_ID_UNIT += (String) data.get(2) + "'";

							rs_unit = st_unit.executeQuery(SELECT_ID_UNIT);
							rs_unit.next();

							prpdStmt_tw_material_2 = (PreparedStatement) connection.prepareStatement(SELECT_DUPLICATE);
							prpdStmt_tw_material_2.setString(1, (String) data.get(1));

							try {
								prpdStmt_tw_material_2.setDouble(2, (double) data.get(4));
							} catch (ClassCastException e) {
								e.getMessage();
							}

							rs_tw_material = prpdStmt_tw_material_2.executeQuery();

							rs_tw_material.next();

							double price_per_unit = rs_tw_material.getDouble("price_per_unit");

							if (Double.compare(price_per_unit, (double) data.get(4)) != 0) {

								prpdStmt_tw_material.setString(1, (String) data.get(1));
								prpdStmt_tw_material.setDouble(2, (Double) data.get(4));
								prpdStmt_tw_material.setInt(3, rs_unit.getInt("id"));
								prpdStmt_tw_material.execute();
								connection.commit();

								arrayQueryS.add(id_work);
								arrayQueryS.add(rs_tw_material.getInt("id"));
								arrayQueryS.add(rs_unit.getInt("id"));
								arrayQueryS.add(data.get(3));
								arrayQueryS.add(rs_tw_material.getInt("id"));
								arrayQueryS.add(data.get(5));
								arrayQueryS.add(data.get(6));
								arrayQueryS.add(0);
								arrayQueryS.add(typeHome);

							} else {
								arrayQueryS.add(id_work);
								arrayQueryS.add(rs_tw_material.getInt("id"));
								arrayQueryS.add(rs_unit.getInt("id"));
								arrayQueryS.add(data.get(3));
								arrayQueryS.add(rs_tw_material.getInt("id"));
								arrayQueryS.add(data.get(5));
								arrayQueryS.add(data.get(6));
								arrayQueryS.add(0);
								arrayQueryS.add(typeHome);
							}

						} catch (SQLException e1) {

							// e1.printStackTrace();
							if (e1.getMessage().equals("Illegal operation on empty result set.")) {
								try {
									// String chek_duplicate = "select * from
									// tw_material where name_material = ? AND
									// price_per_unit = ?";

									prpdStmt_tw_material.setString(1, (String) data.get(1));
									prpdStmt_tw_material.setDouble(2, (Double) data.get(4));
									prpdStmt_tw_material.setInt(3, rs_unit.getInt("id"));
									prpdStmt_tw_material.execute();
									connection.commit();

									prpdStmt_tw_material_2 = (PreparedStatement) connection
											.prepareStatement(SELECT_DUPLICATE);
									prpdStmt_tw_material_2.setString(1, (String) data.get(1));
									prpdStmt_tw_material_2.setDouble(2, (double) data.get(4));

									rs_tw_material = prpdStmt_tw_material_2.executeQuery();
									rs_tw_material.next();

									arrayQueryS.add(id_work);
									arrayQueryS.add(rs_tw_material.getInt("id"));
									arrayQueryS.add(rs_unit.getInt("id"));
									arrayQueryS.add(data.get(3));
									arrayQueryS.add(rs_tw_material.getInt("id"));
									arrayQueryS.add(data.get(5));
									arrayQueryS.add(data.get(6));
									arrayQueryS.add(0);
									arrayQueryS.add(typeHome);

								} catch (SQLException e) {
									if (e1.getErrorCode() != 1062)
										e1.printStackTrace();
								}
							} else if (e1.getErrorCode() != 1062)
								e1.printStackTrace();

						}
					}
					if (data.size() < 4) {
						try {
							prpdStmt_tw_material.setString(1, (String) data.get(1));
							prpdStmt_tw_material.setDouble(2, 0);
							prpdStmt_tw_material.setInt(3, 2);
							prpdStmt_tw_material.execute();
							connection.commit();
						} catch (SQLException e1) {
							if (e1.getErrorCode() != 1062)
								e1.printStackTrace();
						}
					}

				}
				if (arrayQueryS.size() > 0)
					arrayQueryF.add(arrayQueryS);
			}

			for (LinkedList<Object> object : arrayQueryF) {
				// @SuppressWarnings("unchecked")
				// ArrayList<Object> arrayList = (ArrayList<Object>) object;

				try {
					prpdStmt_tw_report.setInt(1, (int) object.get(0));
					prpdStmt_tw_report.setInt(2, (int) object.get(1));
					prpdStmt_tw_report.setInt(3, (int) object.get(2));
					prpdStmt_tw_report.setDouble(4, (double) object.get(3));
					prpdStmt_tw_report.setInt(5, (int) object.get(4));
					prpdStmt_tw_report.setDouble(6, (double) object.get(5));
					prpdStmt_tw_report.setDouble(7, (double) object.get(6));
					prpdStmt_tw_report.setString(8, (String) object.get(8));
					prpdStmt_tw_report.setDate(9, new java.sql.Date(date.getTime()));
					// prpdStmt_tw_report.setInt(10, dates[1]);
					// prpdStmt_tw_report.setInt(11, dates[2]);
					prpdStmt_tw_report.execute();
					connection.commit();
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}
			arrayQueryF.clear();
			arrayQueryF = new LinkedList<>();

		} catch (SQLException e) {
			e.printStackTrace();
			return "ERROR";
		}
		connection = null;
		return "OK";
	}

	public Object[] exportFromDB(String exportQuery) {
		arrayQueryS = new LinkedList<>();
		arrayQueryF = new LinkedList<>();
		try {
			if (connection == null || connection.isClosed())
				connection = (Connection) DriverManager.getConnection(url_database, user_database, password_database);

			statement_select_report = (Statement) connection.createStatement();
			resultSet_select_report = statement_select_report.executeQuery(exportQuery);

			while (resultSet_select_report.next()) {

				arrayQueryS.add(resultSet_select_report.getInt("id"));
				arrayQueryS.add(resultSet_select_report.getString("name_work"));
				arrayQueryS.add(resultSet_select_report.getString("name_material"));
				arrayQueryS.add(resultSet_select_report.getString("name_unit"));
				arrayQueryS.add(resultSet_select_report.getInt("amount"));
				arrayQueryS.add(resultSet_select_report.getDouble("price_per_unit"));
				arrayQueryS.add(resultSet_select_report.getDouble("cost"));
				arrayQueryS.add(resultSet_select_report.getDouble("cost_with_VAT"));
				arrayQueryS.add(resultSet_select_report.getString("type_home"));
				arrayQueryS.add(resultSet_select_report.getDate("date"));

				arrayQueryF.add(arrayQueryS);
				arrayQueryS = new LinkedList<>();
			}

			resultSet_select_report.close();
			statement_select_report.close();
			connection.close();
			return new Object[] { "OK", arrayQueryF };

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return new Object[] { "ERROR", null };
		}

	}
}
