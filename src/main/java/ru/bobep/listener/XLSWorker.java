package ru.bobep.listener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;

public class XLSWorker {

	private HSSFWorkbook workbook;
	private HSSFSheet sheet;
	private HSSFRow row;
	private HSSFCell cell;
	private Iterator<Row> rows;
	private Iterator<Cell> cells;
	private HSSFFont font;
	private HSSFCellStyle style;
	private HSSFDataFormat dataFormat;

	private HSSFRow rowT;
	private HSSFCellStyle styleT;

	private LinkedList<LinkedList<Object>> sheetData = new LinkedList<>();
	private LinkedList<Object> data = new LinkedList<>();
	private LinkedList<LinkedList<Object>> tmpp;

	private Object[] resAr = new Object[2];

	public String openFile(String patchToFile) {
		try {
			workbook = new HSSFWorkbook(new FileInputStream(new File(patchToFile)));
			return "OK";
		} catch (IOException e) {
			if (e.getMessage() != null)
				return e.getMessage();
			else
				return "ERROR";
		}
	}

	public String saveFile(String patchToFile) {
		try {
			FileOutputStream fileOutputStream = new FileOutputStream(new File(patchToFile));
			workbook.write(fileOutputStream);
			fileOutputStream.close();
			// workbook = new HSSFWorkbook(new FileInputStream(new
			// File(patchToFile)));
			return "OK";
		} catch (IOException e) {
			if (e.getMessage() != null)
				return e.getMessage();
			else
				return "ERROR";
		}
	}

	public Object[] getDataFromExcel() {
		try {
			sheetData = new LinkedList<>();
			sheet = workbook.getSheetAt(0);
			rows = sheet.iterator();

			while (rows.hasNext()) {
				row = (HSSFRow) rows.next();
				cells = row.cellIterator();
				data = new LinkedList<>();

				while (cells.hasNext()) {
					cell = (HSSFCell) cells.next();

					switch (cell.getCellType()) {
					case Cell.CELL_TYPE_BLANK:
						continue;
					case Cell.CELL_TYPE_BOOLEAN:
						data.add(String.valueOf(cell.getBooleanCellValue()));
						break;
					case Cell.CELL_TYPE_ERROR:
						data.add(String.valueOf(cell.getErrorCellValue()));
						break;
					case Cell.CELL_TYPE_FORMULA:
						int icellA = cell.getCachedFormulaResultType();
						if ((icellA == Cell.CELL_TYPE_NUMERIC)) {
							data.add(cell.getNumericCellValue());
						}
						break;
					case Cell.CELL_TYPE_NUMERIC:
						data.add(cell.getNumericCellValue());
						break;
					case Cell.CELL_TYPE_STRING:
						data.add(cell.getStringCellValue());
						break;

					default:
						data.add(String.valueOf(cell));
						break;
					}
				}
				if (data.size() != 0)
					sheetData.add(data);
			}

			resAr[0] = "OK";
			resAr[1] = sheetData;
			return resAr;

		} catch (Throwable e) {
			resAr[0] = "ERROR";
			resAr[1] = null;
			return resAr;
		}

	}

	public String setDataToExcel(LinkedList<LinkedList<Object>> datas, String patchToFile) {
		workbook = new HSSFWorkbook();

		sheet = workbook.createSheet("Compare");

		row = sheet.createRow((short) 0);

		tmpp = testA(datas);

		String[] headName = { "Наименование материала", "Ед.изм.", "Кол-во", "Цена за ед. изм,руб.", "Стоимость, руб",
				"Стоимость с НДС,руб", "Тип дома", "Дата заказа", "Разница в кол-ве", "Разница в цене за ед. изм.",
				"Разница в стоимости", "Разница в стоимости c НДС" };
		int i = 0;
		for (String string : headName) {

			cell = row.createCell(i);
			cell.setCellValue(string);
			cell.setCellType(HSSFCell.CELL_TYPE_STRING);
			cell.setCellStyle(getMyCellStyle(null, true, false));
			i++;
		}

		LinkedList<Object> tmp = null;
		int h = 0;
		int noGoIn = 0;
		for (i = 0; i < datas.size(); i++) {
			LinkedList<Object> linkedList = datas.get(i);

			int k = 0;
			int kk = i;

			if (tmp == null || !tmp.get(1).equals(linkedList.get(1))) {

				while (sheet.getRow((short) kk) != null) {
					kk++;
					k++;
				}

				row = sheet.createRow((short) i + k);

				cell = row.createCell(0);
				cell.setCellValue((String) linkedList.get(1));
				cell.setCellType(HSSFCell.CELL_TYPE_STRING);
				cell.setCellStyle(getMyCellStyle(null, true, true));
				row = sheet.createRow((short) i + k + 1);

			} else {
				while (sheet.getRow((short) kk) != null) {
					kk++;
					k++;
				}

				row = sheet.createRow((short) i + k);
			}

			for (int j = 0; j < linkedList.size(); j++) {

				// [$-F800]ДДДД, ММММ ДД, ГГГГ
				// # ##0,00\ \₽
				sheet.autoSizeColumn(j);
				// dataFormat.getFormat("Основной");
				// dataFormat.getFormat("#,##0.00 ₽");
				// dataFormat.getFormat("# ##0,00");
				// dataFormat.getFormat("$-F800]ДДДД, ММММ ДД, ГГГГ");
				switch (j) {
				case 2:
					cell = row.createCell(0);
					cell.setCellValue((String) linkedList.get(j));
					cell.setCellType(HSSFCell.CELL_TYPE_STRING);
					cell.setCellStyle(getMyCellStyle(null, false, false));
					break;
				case 3:
					cell = row.createCell(1);
					cell.setCellValue((String) linkedList.get(j));
					cell.setCellType(HSSFCell.CELL_TYPE_STRING);
					cell.setCellStyle(getMyCellStyle(null, false, false));
					break;
				case 4:
					cell = row.createCell(2);
					if (linkedList.get(j) instanceof Integer)
						cell.setCellValue((int) linkedList.get(j));
					else
						cell.setCellValue((double) linkedList.get(j));

					cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
					cell.setCellStyle(getMyCellStyle("#,##0.00", false, false));
					break;
				case 5:
					cell = row.createCell(3);
					cell.setCellValue((double) linkedList.get(j));
					cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
					cell.setCellStyle(getMyCellStyle("#,##0.00 ₽", false, false));
					break;
				case 6:
					cell = row.createCell(4);
					cell.setCellValue((double) linkedList.get(j));
					cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
					cell.setCellStyle(getMyCellStyle("#,##0.00 ₽", false, false));
					break;
				case 7:
					cell = row.createCell(5);
					cell.setCellValue((double) linkedList.get(j));
					cell.setCellType(HSSFCell.CELL_TYPE_NUMERIC);
					cell.setCellStyle(getMyCellStyle("#,##0.00 ₽", false, false));
					break;
				case 8:
					cell = row.createCell(6);
					cell.setCellValue((String) linkedList.get(j));
					cell.setCellType(HSSFCell.CELL_TYPE_STRING);
					cell.setCellStyle(style);
					break;
				case 9:
					cell = row.createCell(7);
					cell.setCellValue((Date) linkedList.get(j));
					// cell.setCellType(HSSFCell.CELL_TYPE_STRING);
					cell.setCellStyle(getMyCellStyle("[$-F800]ДДДД, ММММ ДД, ГГГГ", false, false));

					noGoIn++;
					if (noGoIn == 2) {
						if (String.valueOf(sheet.getRow(cell.getRowIndex() - 1).getCell(0))
								.equals(String.valueOf(sheet.getRow(cell.getRowIndex()).getCell(0)))) {

							LinkedList<Object> test = tmpp.get(h);

							rowT = sheet.getRow(cell.getRowIndex() - 1);
							for (int l = 0; l < test.size(); l++) {

								sheet.autoSizeColumn(9 + l);
								cell = rowT.createCell((short) 7 + l + 1);

								try {
									cell.setCellValue(Double.parseDouble(String.valueOf(test.get(l))));
								} catch (NumberFormatException e) {
									cell.setCellValue(String.valueOf(test.get(l)));
								}

								if (cell.getColumnIndex() != 8)
									styleT = getMyCellStyle("#,##0.00 ₽", false, false);
								else
									styleT = getMyCellStyle("#,##0.00", false, false);
								styleT.setVerticalAlignment(CellStyle.VERTICAL_CENTER);

								cell.setCellStyle(styleT);

								row.createCell(7 + l + 1).setCellStyle(getMyCellStyle(null, false, false));

								sheet.addMergedRegion(new CellRangeAddress(cell.getRowIndex(), cell.getRowIndex() + 1,
										cell.getColumnIndex(), cell.getColumnIndex()));

							}
							noGoIn = 0;
							h++;
						}
					}
					break;

				default:
					break;
				}

			}

			tmp = linkedList;

		}

		saveFile(patchToFile);
		return "OK";
	}

	private LinkedList<LinkedList<Object>> testA(LinkedList<LinkedList<Object>> datas) {

		LinkedList<LinkedList<Object>> tttt = new LinkedList<>();
		LinkedList<Object> linkedList = new LinkedList<>();
		int c = 0;
		LinkedList<Object> tmpA;
		LinkedList<Object> tmpB;
		for (int i = 0; i < datas.size(); i++) {
			if (i == 0) {
				c = i + 1;
				if (c > datas.size())
					break;
				tmpA = datas.get(i);
				tmpB = datas.get(i + 1);

			} else {
				c = i + i + 1;
				if (c > datas.size())
					break;
				tmpA = datas.get(i + i);
				tmpB = datas.get(i + i + 1);

			}
			tmpA.get(4);
			tmpA.get(5);
			tmpA.get(6);
			tmpA.get(7);

			tmpB.get(4);
			tmpB.get(5);
			tmpB.get(6);
			tmpB.get(7);

			double abs = Math.abs(
					Double.parseDouble(String.valueOf(tmpA.get(4))) - Double.parseDouble(String.valueOf(tmpB.get(4))));
			linkedList.add(Double.compare(abs, 0) == 0 && Double.compare(abs, 0.0) == 0
					? "Разница в количестве отсутствует" : abs);

			abs = Math.abs(
					Double.parseDouble(String.valueOf(tmpA.get(5))) - Double.parseDouble(String.valueOf(tmpB.get(5))));
			linkedList.add(Double.compare(abs, 0) == 0 && Double.compare(abs, 0.0) == 0
					? "Разница в цене за ед. изм. отсутствует" : abs);

			abs = Math.abs(
					Double.parseDouble(String.valueOf(tmpA.get(6))) - Double.parseDouble(String.valueOf(tmpB.get(6))));
			linkedList.add(Double.compare(abs, 0) == 0 && Double.compare(abs, 0.0) == 0
					? "Разница в стоимости отсутствует" : abs);

			abs = Math.abs(
					Double.parseDouble(String.valueOf(tmpA.get(7))) - Double.parseDouble(String.valueOf(tmpB.get(7))));
			linkedList.add(Double.compare(abs, 0) == 0 && Double.compare(abs, 0.0) == 0
					? "Разница в стоимости c НДС отсутствует" : abs);

			tttt.add(linkedList);
			linkedList = new LinkedList<>();
		}

		return tttt;
	}

	/**
	 * 
	 * @param dataFormatString
	 * @param fontBold
	 * @param fontItalic
	 * @return {@link HSSFCellStyle}
	 */
	private HSSFCellStyle getMyCellStyle(String dataFormatString, boolean fontBold, boolean fontItalic) {

		font = workbook.createFont();
		font.setFontName("Verdana");
		font.setFontHeightInPoints((short) 8);
		font.setBold(fontBold);
		font.setItalic(fontItalic);

		style = workbook.createCellStyle();
		style.setBorderBottom(CellStyle.BORDER_THIN);
		style.setBorderTop(CellStyle.BORDER_THIN);
		style.setBorderRight(CellStyle.BORDER_THIN);
		style.setBorderLeft(CellStyle.BORDER_THIN);
		style.setFont(font);
		if (dataFormatString != null) {
			dataFormat = workbook.createDataFormat();
			style.setDataFormat(dataFormat.getFormat(dataFormatString));
		}
		return style;
	}
}
