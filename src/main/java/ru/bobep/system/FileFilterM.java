package ru.bobep.system;

import java.io.File;

import javax.swing.filechooser.FileFilter;

public class FileFilterM extends FileFilter {

	private String fileExtension;
	private String fileDescription;

	public FileFilterM(String fileExtension, String fileDescription) {
		this.fileExtension = fileExtension;
		this.fileDescription = fileDescription;
	}

	@Override
	public boolean accept(File file) {
		return file.isDirectory() || file.getAbsolutePath().endsWith(fileExtension);
	}

	@Override
	public String getDescription() {
		return fileDescription + " (*." + fileExtension + ")";
	}

	public String getExtension() {
		return fileExtension;

	}

}
